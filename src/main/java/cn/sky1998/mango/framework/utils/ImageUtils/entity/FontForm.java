package cn.sky1998.mango.framework.utils.ImageUtils.entity;

import lombok.Data;

import java.awt.*;

/**
 * 字体参数
 * @author tcy
 * @Date 06-12-2022
 */
@Data
public class FontForm {

    /**
     * 字
     */
    private String title;

    /**
     * 字体颜色
     */
    private Color color;

    private String name="华文隶书";

    private Integer style=PLAIN;
    /**
     * 字体大小
     */
    private Integer size;

    private Integer x;

    private Integer y;

    /**
     * 字体样式普通
     */
    public static final int PLAIN       = 0;

    /**
     * 字体样式加粗
     */
    public static final int BOLD        = 1;

    /**
     * 字体样式斜体
     */
    public static final int ITALIC      = 2;
}