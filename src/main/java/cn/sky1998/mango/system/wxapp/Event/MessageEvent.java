package cn.sky1998.mango.system.wxapp.Event;

import org.springframework.stereotype.Component;

/**
 * 普通消息事件-同步
 * @author tcy
 * @Date 12-12-2022
 */
@Component
public class MessageEvent{

    /**
     * 根据用户发送内容回复消息
     * @param openid
     * @param msg
     * @return
     */
    public String reciveMessage(String openid,String msg){

        /**
         * 实现相应的业务
         */


        return "设置成功，小程序可正常使用!";
        }


}