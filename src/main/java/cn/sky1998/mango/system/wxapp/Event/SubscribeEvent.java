package cn.sky1998.mango.system.wxapp.Event;

import org.springframework.context.ApplicationEvent;

import java.time.Clock;

/**
 * 关注/取关消息事件
 * @author tcy
 * @Date 18-02-2022
 */
public class SubscribeEvent extends ApplicationEvent {

    private String openid;

    private String msg;


    public SubscribeEvent(Object source) {
        super(source);
    }

    public SubscribeEvent() {
        super(new Object());
    }

    public SubscribeEvent(String openid,String msg) {
        this();
        this.openid = openid;
        this.msg = msg;
    }

    public SubscribeEvent(Object source, Clock clock, String openid) {
        super(source, clock);
        this.openid = openid;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

}