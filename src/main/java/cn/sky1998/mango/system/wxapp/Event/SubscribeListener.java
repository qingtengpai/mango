package cn.sky1998.mango.system.wxapp.Event;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

/**
 * @author tcy
 * @Date 22-02-2022
*/
@Component
public class SubscribeListener implements ApplicationListener<SubscribeEvent> {


    @Async
    @Override
    public void onApplicationEvent(SubscribeEvent event) {
       if (event.getMsg().equals("subscribe")){
           //用户关注

       }else if (event.getMsg().equals("unsubscribe")){
         //用户取关
       }

    }
}