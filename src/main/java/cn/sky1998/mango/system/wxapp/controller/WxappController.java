package cn.sky1998.mango.system.wxapp.controller;

import cn.sky1998.mango.framework.web.core.AjaxResult;
import cn.sky1998.mango.system.wxapp.service.WxappService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
@RequestMapping("/wechat")
public class WxappController {

    @Autowired
    private WxappService wxappService;

    /**
     * 公众号-获取二维码
     * @return
     */
    @GetMapping("/getQrCode/public")
    public AjaxResult getQrCode(String openid){
        return AjaxResult.success(wxappService.getQrCode(openid));
    }

    /**
     * 验证token-公众号
     * @param response
     * @param signature
     * @param timestamp
     * @param nonce
     * @param echostr
     * @return
     * @throws IOException
     */
    @RequestMapping("/valid/public")
    public String validateWxappToken(HttpServletResponse response,String  signature, String timestamp, String nonce, String echostr) throws IOException {
        String s = wxappService.validateWxappToken(signature, timestamp, nonce, echostr);
        response.setCharacterEncoding("UTF-8");
        response.getOutputStream().print(echostr);
        return s;
    }

    /**
     * 网页授权后跳转的地址-测试使用
     * @param code
     */
    @GetMapping("/auth/public")
    public void auth(@RequestParam("code") String code) {
        System.out.println(code);
    }


    /**
     * 接收用户的消息和事件
     * @param request
     * @param openid
     * @return
     */
    @PostMapping("/valid/public")
    public String getMessage(HttpServletRequest request,String  openid) {

        return wxappService.getMessageAndSedMessage(request,openid);
    }

    /**
     * 获取小程序用户openid
     * @param code
     * @param appid
     * @param req
     * @return
     */
    @PostMapping(value = "/getOpenid/public")
    public AjaxResult getOpenid(String code, String appid,HttpServletRequest req){

        return AjaxResult.success(wxappService.getOpenid(code,appid));
    }

    /**
     * 获取公众号用户信息
     * @param code
     * @param req
     * @return
     */
    @PostMapping(value = "/getUserInfo/public")
    public AjaxResult getUserInfo(String code,HttpServletRequest req){

        return AjaxResult.success(wxappService.getUserInfo(code));
    }

    /**
     * 获取小程序页面二维码
     * @return
     */
    @PostMapping("/createQRCode/public")
    public AjaxResult createQRCode(String scent,String page,String appid){
        return AjaxResult.success(wxappService.createQRCode(scent,page,appid));
    }



}
