package cn.sky1998.mango.system.wxapp.domain;

import lombok.Data;

/**
 * 草稿
 * @author tcy
 * @Date 22-02-2023
 */
@Data
public class DraftArticle {

    private String title;
    private String author;
    //摘要
    private String digest;
    private String content;
    //阅读原文后url
    private String content_source_url;
    //图文消息的封面图片素材id（必须是永久MediaID）
    private String thumb_media_id;
    //是否打开评论
    private String need_open_comment;
    //Uint32 是否粉丝才可评论，0所有人可评论(默认)，1粉丝才可评论
    private String only_fans_can_comment;
}