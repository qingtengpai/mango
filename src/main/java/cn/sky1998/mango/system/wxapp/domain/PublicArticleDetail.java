package cn.sky1998.mango.system.wxapp.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

import java.util.List;

/**
 * @author tcy
 * @Date 23-02-2023
 */
@Data
public class PublicArticleDetail {

    @XStreamAlias("count")
    String count;

    @XStreamAlias("item")
    List<PublicArticleDetailItem> item;
}