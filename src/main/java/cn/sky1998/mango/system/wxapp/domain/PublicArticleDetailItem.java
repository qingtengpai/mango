package cn.sky1998.mango.system.wxapp.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

/**
 * @author tcy
 * @Date 23-02-2023
 */
@Data
public class PublicArticleDetailItem {

    @XStreamAlias("idx")
    String idx;

    @XStreamAlias("article_url")
    String article_url;
}