package cn.sky1998.mango.system.wxapp.domain;

import lombok.Data;

/**
 * @author tcy
 * @Date 17-02-2023
 */
@Data
public class PublicUserInfo {

    String subscribe;
    String  openid;
    String  language;
    String  subscribe_time;
    String  unionid;
    String  remark;
    String  groupid;
    String  tagid_list;
    String  subscribe_scene;
    String  qr_scene;
    String  qr_scene_str;
}