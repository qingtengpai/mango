package cn.sky1998.mango.system.wxapp.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

import java.util.List;

/**
 * @author tcy
 * @Date 23-02-2023
 */
@Data
public class PublishEventInfo {

    @XStreamAlias("publish_id")
    private String publish_id;

    @XStreamAlias("publish_status")
    private String publish_status;

    @XStreamAlias("article_id")
    private String article_id;

    @XStreamAlias("article_detail")
    private String article_detail;

}