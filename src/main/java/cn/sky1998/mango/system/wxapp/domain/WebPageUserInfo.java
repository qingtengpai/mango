package cn.sky1998.mango.system.wxapp.domain;

import lombok.Data;

/**
 * @author tcy
 * @Date 17-02-2023
 */
@Data
public class WebPageUserInfo {
    String openid;
    String  nickname;
    String   sex;
    String    province;
    String    city;
    String    country;
    String    headimgurl;
    String    privilege;
    String    unionid;
}