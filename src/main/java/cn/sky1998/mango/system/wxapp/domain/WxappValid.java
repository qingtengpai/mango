package cn.sky1998.mango.system.wxapp.domain;

import lombok.Data;

/**
 * @author tcy
 * @Date 07-12-2022
 */
@Data
public class WxappValid {
    private String appid;
    private String code;
}