package cn.sky1998.mango.system.wxapp.service;

import cn.sky1998.mango.system.wxapp.domain.DraftArticle;
import com.alibaba.fastjson.JSONObject;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


public interface PublicNumService {

    /**
     * 新增素材
     * @return
     */
    Map<String,String> publishMedia();

    /**
     * 查询素材
     * @return
     */
    Map<String,String> getMedia(String media_id);

    /**
     * 新增草稿
     * @return
     */
    JSONObject publishDraft(List<DraftArticle> draftArticles);

    /**
     * 发布文章
     * @return
     */
    JSONObject publishFree(String media_id);

    /**
     * 查询发布状态
     * @return
     */
    JSONObject queryPublishFree(String publish_id);

    /**
     * 查询文章信息
     * @return
     */
    JSONObject getArticle(String article_id);

    /**
     * 删除文章
     * @return
     */
    JSONObject delArticle(String article_id);
}
