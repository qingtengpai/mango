package cn.sky1998.mango.system.wxapp.service.impl;

import cn.sky1998.mango.common.utils.http.HttpUtils;
import cn.sky1998.mango.system.wxapp.WxappUtils;
import cn.sky1998.mango.system.wxapp.domain.DraftArticle;
import cn.sky1998.mango.system.wxapp.service.PublicNumService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author tcy
 * @Date 22-02-2023
 */
@Service
public class PublicNumServiceImpl implements PublicNumService {

    @Autowired
    private WxappUtils wxappUtils;

    @Override
    public Map<String, String> publishMedia() {
        return null;
    }

    @Override
    public Map<String, String> getMedia(String media_id) {

        return null;
    }

    @Override
    public JSONObject publishDraft(List<DraftArticle> draftArticles) {
        String url="https://api.weixin.qq.com/cgi-bin/draft/add?access_token=";
        String s = JSONObject.toJSONString(draftArticles);
        JSONArray array = JSON.parseArray(s);
        JSONObject params = new JSONObject();
        params.put("articles",array);
        JSONObject jsonObject= HttpUtils.doPost(url + wxappUtils.getAccessToken(),params);
        return jsonObject;
    }

    /**
     * 发布图文
     * @return
     */
    @Override
    public JSONObject publishFree(String media_id) {
        String url="https://api.weixin.qq.com/cgi-bin/freepublish/submit?access_token=";

        JSONObject params = new JSONObject();
        params.put("articles",media_id);
        JSONObject jsonObject= HttpUtils.doPost(url + wxappUtils.getAccessToken(),params);
        return jsonObject;
    }

    @Override
    public JSONObject queryPublishFree(String publish_id) {
        String url="https://api.weixin.qq.com/cgi-bin/freepublish/get?access_token=";

        JSONObject params = new JSONObject();
        params.put("publish_id",publish_id);
        JSONObject jsonObject= HttpUtils.doPost(url + wxappUtils.getAccessToken(),params);
        return jsonObject;
    }

    @Override
    public JSONObject getArticle(String article_id) {
        String url="https://api.weixin.qq.com/cgi-bin/freepublish/getarticle?access_token=";

        JSONObject params = new JSONObject();
        params.put("article_id",article_id);
        JSONObject jsonObject= HttpUtils.doPost(url + wxappUtils.getAccessToken(),params);
        return jsonObject;
    }

    @Override
    public JSONObject delArticle(String article_id) {
        String url="https://api.weixin.qq.com/cgi-bin/freepublish/delete?access_token=";

        JSONObject params = new JSONObject();
        params.put("article_id",article_id);
        JSONObject jsonObject= HttpUtils.doPost(url + wxappUtils.getAccessToken(),params);
        return jsonObject;
    }
}