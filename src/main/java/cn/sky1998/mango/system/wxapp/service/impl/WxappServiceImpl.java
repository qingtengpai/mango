package cn.sky1998.mango.system.wxapp.service.impl;

import cn.sky1998.mango.common.exception.CustomException;
import cn.sky1998.mango.common.utils.http.HttpUtils;
import cn.sky1998.mango.framework.utils.SpringUtils;
import cn.sky1998.mango.system.wxapp.Event.MessageEvent;
import cn.sky1998.mango.system.wxapp.Event.SubscribeEvent;
import cn.sky1998.mango.system.wxapp.WxappUtils;
import cn.sky1998.mango.system.wxapp.XmlUtils;
import cn.sky1998.mango.system.wxapp.domain.*;
import cn.sky1998.mango.system.wxapp.service.WxappService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 微信公众号相关接口
 *
 * @author tcy@1753163342@qq.com
 * @Date 15-12-2021
 */
@Service
public class WxappServiceImpl implements WxappService {

    @Value("${mango.wxapp.gzh.token}")
    private static String token;

    @Value("${mango.wxapp.gzh.appid}")
    private  String appid;
    @Value("${mango.wxapp.gzh.secret}")
    private  String secret;

    private Logger log = LoggerFactory.getLogger(WxappServiceImpl.class);

    private static RestTemplate restTemplate = (RestTemplate) SpringUtils.getBeanByName("restTemplate");

    private String qrCodeUrl = "https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token=%s";

    private static final String AUTHORIZATION_CODE = "authorization_code";

    //获取小程序openid的接口
    private static final String REQUEST_URL = "https://api.weixin.qq.com/sns/jscode2session";

    //获取公众号用户信息
    private static final String REQUEST_GETUNSEINFO="https://api.weixin.qq.com/cgi-bin/user/info?access_token=";

    //获取网页用户信息
    private static final String REQUEST_GETUNSEINFO_WEBPAGE="https://api.weixin.qq.com/sns/userinfo?access_token=";

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private WxappUtils wxappUtils;

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private MessageEvent messageEvent;

    /**
     * 获取登录二维码
     *
     * @return
     */
    @Override
    public Map<String, Object> getQrCode(String openid) {
        ///获取临时二维码
        String url = String.format(qrCodeUrl, wxappUtils.getAccessToken());
        ResponseEntity<String> result = restTemplate.postForEntity(url,
                "{\"expire_seconds\": 604800, \"action_name\": \"QR_STR_SCENE\", \"action_info\": {\"scene\": {\"scene_str\": "+openid+"}}}", String.class);
        log.info("二维码:{}", result.getBody());
        JsonNode jsonNode;
        try {
            jsonNode = objectMapper.readTree(result.getBody());
        } catch (JsonProcessingException e) {
            throw new CustomException("获取二维码异常");
        }
        Map<String, Object> map = new HashMap<>();
        map.put("ticket", jsonNode.get("ticket"));
        map.put("url", jsonNode.get("url"));
        return map;

    }

    /**
     * 检查是否登录
     *
     * @param ticket
     * @return
     */
    @Override
    public String checkLogin(String ticket) {
        return null;
    }

    /**
     * 微信回调
     *
     * @param request
     * @return
     */
    @Override
    public String handleWxEvent(HttpServletRequest request) throws IOException {
        //字符输入流
        ServletInputStream inputStream = request.getInputStream();
        //转换成字符输出流
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        bufferedReader.readLine();
        return null;
    }

    /**
     * 验证微信token
     *
     * @param signature
     * @param timestamp
     * @param nonce
     * @param echostr
     * @return
     */
    @Override
    public String validateWxappToken(String signature, String timestamp, String nonce, String echostr) {

//        String tmpStr = wxappUtils.getSHA1(token,  timestamp,  nonce);
//        System.out.println(tmpStr+"*****************");
//        if (tmpStr.equals(signature.toUpperCase())) {
//            return echostr;
//        }
        System.out.println(echostr);
        return echostr;
    }

    @Override
    public String getMessageAndSedMessage(HttpServletRequest request, String openid) {

        //用来接收微信下发的xml信息
        BufferedReader bufferedReader = null;
        StringBuffer stringBuffer = null;
        String str;
        try {
            bufferedReader = request.getReader();
            stringBuffer = new StringBuffer();
            while ((str = bufferedReader.readLine()) != null) {
                stringBuffer.append(str);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        //用户信息（xml）
        String msg = stringBuffer.toString();

        ContentMsg contentMsg = wxappUtils.receiveContentMsg(msg);

        //根据消息类型处理 普通消息、关注、菜单消息事件
        String handleText = null;
        if (contentMsg.getMsgType().equals("text")) {
            //发消息事件-发布图文推送
            handleText = handleText(contentMsg,openid);
        } else if (contentMsg.getMsgType().equals("event")) {
            handleText = handleEvent(contentMsg,openid);
        }

        return handleText;

    }

    /**
     * 处理事件（关注、菜单点击）
     *
     * @param contentMsg
     */
    public String handleEvent(ContentMsg contentMsg,String openid) {
        //关注事件
        if (contentMsg.getEvent().equals("subscribe")){

            //确定是主动关注还是扫码关注
            //事件 KEY 值，qrscene_为前缀，后面为二维码的参数值
            contentMsg.getEventKey();
            contentMsg.getTicket();

            //注意 这边的 FromUserName 和 ToUserName 需要对调一下 因为现在是服务器发送给用户了
            String temp = contentMsg.getFromUserName();
            contentMsg.setFromUserName(contentMsg.getToUserName());
            contentMsg.setToUserName(temp);
            contentMsg.setMsgType("text");
            contentMsg.setCreateTime(new Date().getTime() + "");

            contentMsg.setContent("欢迎关注,小程序授权发送appid,如：appid=wxb0742e21010b6f03");

            //调用关注后业务-------------异步
            SubscribeEvent subscribeEvent=new SubscribeEvent(openid,"subscribe");
            applicationContext.publishEvent(subscribeEvent);

            return XmlUtils.parseToXml(contentMsg);

        //取消关注
        }else if (contentMsg.getEvent().equals("unsubscribe")){
            //调用关注后业务-------------异步
            SubscribeEvent subscribeEvent=new SubscribeEvent(openid,"unsubscribe");
            applicationContext.publishEvent(subscribeEvent);
        } else if (contentMsg.getEvent().equals("CLICK")){

            ContentMsg contentMsg1=new ContentMsg();
            //注意 这边的 FromUserName 和 ToUserName 需要对调一下 因为现在是服务器发送给用户了

            contentMsg1.setFromUserName(contentMsg.getToUserName());
            contentMsg1.setToUserName(contentMsg.getFromUserName());
            contentMsg1.setCreateTime(new Date().getTime() + "");

            if (contentMsg.getEventKey().equals("zaoan")){
                contentMsg1.setMsgType("news");

                ArrayList<Article> articles = new ArrayList<>();
                Article article=new Article();
                article.setTitle("图文消息标题");
                article.setDescription("图文消息描述");
                article.setUrl("https://www.baidu.com");
                article.setPicUrl("https://bat-1300430211.cos.ap-nanjing.myqcloud.com/4kyingshi/203305-15073795856adc.jpg");
                articles.add(article);
                contentMsg1.setArticles(articles);
                contentMsg1.setArticleCount("1");
                return XmlUtils.beanToXml(contentMsg1);
            }else if (contentMsg.getEventKey().equals("connect")){

                contentMsg1.setMsgType("image");
                Image image=new Image();
                image.setMediaId("ZbwMulLSEdrgDt3mvkkjAn6vyeDrW6U1lSBjc7t_44f__3APRIJsraMovR3a_8ot");
                contentMsg1.setImage(image);
                return XmlUtils.beanToXml(contentMsg1);

            }

            //发布文章响应
        }else if (contentMsg.getEvent().equals("PUBLISHJOBFINISH")){

            //获取文章相应信息
        }

        return null;
    }

    /**
     * 处理消息
     *
     * @param contentMsg
     */
    public String handleText(ContentMsg contentMsg,String openid) {

        //注意 这边的 FromUserName 和 ToUserName 需要对调一下 因为现在是服务器发送给用户了
        String temp = contentMsg.getFromUserName();
        contentMsg.setFromUserName(contentMsg.getToUserName());
        contentMsg.setToUserName(temp);
        contentMsg.setMsgType("text");
        contentMsg.setCreateTime(new Date().getTime() + "");

        final String receiveContentMsg = contentMsg.getContent();

        String s = messageEvent.reciveMessage(openid, receiveContentMsg);

        contentMsg.setContent(s);

        return XmlUtils.parseToXml(contentMsg);

    }
    /**
     * 获取openid-小程序
     * @param code
     * @param appid
     * @return
     */
    @Override
    public Map<String, Object> getOpenid(String code, String appid) {
        Map<String, Object> userMap = new HashMap<>();
        //secret 如果是单小程序，appid可以直接配置文件配置
        // 如果是多小程序，保存到小程序信息中。
        String secret="";
        JSONObject sessionKeyOpenId = getSessionKeyOrOpenId(code,appid,secret);

        // 获取openId
        String openId = sessionKeyOpenId.getString("openid");
        userMap.put("openId",openId);

        return userMap;
    }

    /**
     * 公众号获取用户信息(关注公众号信息)
     * @param code
     * @return
     */
    @Override
    public PublicUserInfo getUserInfo(String code) {
        String accessToken = wxappUtils.getAccessTokenByCode(code);
        // 发送post请求读取调用微信接口获取openid用户唯一标识
        String result = HttpUtils.sendGet(REQUEST_GETUNSEINFO+accessToken+"&openid="+appid+"&lang=zh_CN");
        JSONObject jsonObject = JSON.parseObject(result);
        PublicUserInfo userInfo=new PublicUserInfo();

        userInfo.setSubscribe(jsonObject.getString("subscribe"));
        userInfo.setOpenid(jsonObject.getString("openid"));
        userInfo.setLanguage(jsonObject.getString("language"));
        userInfo.setSubscribe_time(jsonObject.getString("subscribe_time"));
        userInfo.setGroupid(jsonObject.getString("groupid"));
        userInfo.setUnionid(jsonObject.getString("unionid"));

        userInfo.setSubscribe_scene(jsonObject.getString("subscribe_scene"));
        userInfo.setRemark(jsonObject.getString("remark"));
        userInfo.setTagid_list(jsonObject.getString("tagid_list"));
        userInfo.setQr_scene(jsonObject.getString("qr_scene"));
        userInfo.setQr_scene_str(jsonObject.getString("qr_scene_str"));

        return userInfo;
    }

    /**
     * 跟据参数和页面生成小程序二维码
     * @param scene
     * @param page
     * @return
     */
    @Override
    public PublicUserInfo createQRCode(String scene, String page,String appid) {
                 String filePath="临时文件";
                 try {
                         //调用微信接口生成二维码
                         URL url = new URL("https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token="+wxappUtils.getAccessTokenByAppidAndSecret(appid,"secret"));
                         HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                         httpURLConnection.setRequestMethod("POST");// 提交模式
                         // conn.setConnectTimeout(10000);//连接超时 单位毫秒
                         // conn.setReadTimeout(2000);//读取超时 单位毫秒
                         // 发送POST请求必须设置如下两行
                         httpURLConnection.setDoOutput(true);
                        httpURLConnection.setDoInput(true);
                         // 获取URLConnection对象对应的输出流
                         PrintWriter printWriter = new PrintWriter(httpURLConnection.getOutputStream());
                         // 发送请求参数
                         JSONObject paramJson = new JSONObject();
                         //这就是你二维码里携带的参数 String型  名称不可变
                         paramJson.put("scene", scene);
                         //注意该接口传入的是page而不是path
                         paramJson.put("page", page);
                         //这是设置扫描二维码后跳转的页面
                         paramJson.put("width", 200);
                         paramJson.put("is_hyaline", true);
                         paramJson.put("auto_color", true);
                         printWriter.write(paramJson.toString());
                         // flush输出流的缓冲
                         printWriter.flush();

                         //开始获取数据
                         BufferedInputStream bis = new BufferedInputStream(httpURLConnection.getInputStream());
                         //创建临时文件
                         OutputStream os = new FileOutputStream(new File(filePath));
                         //上传临时文件

                         //删除临时文件
                         int len;
                         byte[] arr = new byte[1024];
                         while ((len = bis.read(arr)) != -1) {
                             os.write(arr, 0, len);
                                 os.flush();
                             }
                         os.close();
                     } catch (Exception e) {
                         e.printStackTrace();
                     }

                 System.out.println("打开地址查看生成的二维码：" + filePath);
        return null;
    }

    @Override
    public WebPageUserInfo getUserInfoByWebPage(String access_token,String openid) {
        String result = HttpUtils.sendGet(REQUEST_GETUNSEINFO_WEBPAGE+access_token+"&openid="+openid+"&lang=zh_CN");
        JSONObject jsonObject = JSON.parseObject(result);
        WebPageUserInfo pageUserInfo=new WebPageUserInfo();
        pageUserInfo.setOpenid(jsonObject.getString("openid"));
        pageUserInfo.setNickname(jsonObject.getString("nickname"));
        pageUserInfo.setSex(jsonObject.getString("sex"));
        pageUserInfo.setProvince(jsonObject.getString("province"));
        pageUserInfo.setCity(jsonObject.getString("city"));
        pageUserInfo.setCountry(jsonObject.getString("country"));
        pageUserInfo.setHeadimgurl(jsonObject.getString("headimgurl"));
        pageUserInfo.setPrivilege(jsonObject.getString("privilege"));
        pageUserInfo.setUnionid(jsonObject.getString("unionid"));
        return pageUserInfo;
    }

    @Override
    public Map<String, Object> getOpenid(String code, String appid,String secret) {
        Map<String, Object> userMap = new HashMap<>();

        JSONObject sessionKeyOpenId = getSessionKeyOrOpenId(code,appid,secret);

        // 获取openId
        String openId = sessionKeyOpenId.getString("openid");
        userMap.put("openId",openId);

        return userMap;
    }

    /**
     * 根据code获取openid
     * @param code
     * @return
     * @throws Exception
     */
    private JSONObject getSessionKeyOrOpenId(String code, String appid, String secret) {
        Map<String, String> requestUrlParam = new HashMap<>();
        // 小程序appId，自己补充
        requestUrlParam.put("appid", "wxb0742e21010b6f03");
        // 小程序secret，自己补充
        requestUrlParam.put("secret", secret);
        // 小程序端返回的code
        requestUrlParam.put("js_code", code);
        // 默认参数
        requestUrlParam.put("grant_type", AUTHORIZATION_CODE);

        // 发送post请求读取调用微信接口获取openid用户唯一标识
        String result = HttpUtils.doPost(REQUEST_URL, requestUrlParam);

        return JSON.parseObject(result);
    }


}