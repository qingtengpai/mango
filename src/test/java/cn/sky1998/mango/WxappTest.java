package cn.sky1998.mango;

import cn.sky1998.mango.common.utils.http.HttpUtils;
import cn.sky1998.mango.system.wxapp.WxappUtils;
import cn.sky1998.mango.system.wxapp.domain.DraftArticle;
import cn.sky1998.mango.system.wxapp.service.impl.WxappServiceImpl;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author tcy@1753163342@qq.com
 * @Date 15-12-2021
 */
@SpringBootTest
public class WxappTest {


    @Autowired
    private WxappUtils wxappUtils;

    @Autowired
    private WxappServiceImpl wxappService;

    @Test
    public void test1() {

        System.out.println(wxappUtils.sendKfMsg("@小李同学,希望你在新的一年里面快快乐乐的", "oZ2B35vrgE5AwaBdVXjsa1jtavzA"));

    }


    @Test
    public void test2(){
        wxappService.getOpenid("wx48da5cab584f1439","wx48da5cab584f1439","9155ab2e3600232f8b37e21ed820157a");
    }

    /**
     * 创建公众号菜单
     */
    @Test
    public void createMenuPublic(){
       // System.out.println(wxappUtils.getAccessTokenByAppidAndSecret("wxe5137dbfe629216a","5f77e7bbc1783e6a0230c0bc952c9b2c"));
        Map<String, String> requestUrlParam = new HashMap<>();
        requestUrlParam.put("scene", "1");
        String result = HttpUtils.doPost("https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token="+wxappUtils.getAccessTokenByAppidAndSecret("wxb0742e21010b6f03","5f77e7bbc1783e6a0230c0bc952c9b2c"),requestUrlParam);
        JSONObject jsonObject = JSON.parseObject(result);
    }

    /**
     * 发布草稿
     */
    @Test
    public void publishDraft(){
        List<DraftArticle> draftArticles=new ArrayList<>();
        DraftArticle draftArticle=new DraftArticle();
        draftArticle.setContent("1");
        draftArticles.add(draftArticle);
        String s = JSONObject.toJSONString(draftArticles);
        JSONArray array = JSON.parseArray(s);
        JSONObject params = new JSONObject();
        params.put("articles",array);

        JSONObject jsonObject= HttpUtils.doPost("https://api.weixin.qq.com/cgi-bin/draft/add?access_token=" + wxappUtils.getAccessTokenByAppidAndSecret("wxd0c2a6a54163249b", "628792f7bdbe792dc5d940b85aa34e3e"),params);
       // JSONObject jsonObject = RestUtil.post("https://api.weixin.qq.com/cgi-bin/draft/add?access_token=" + wxappUtils.getAccessTokenByAppidAndSecret("wxd0c2a6a54163249b", "628792f7bdbe792dc5d940b85aa34e3e"), params);
        System.out.println(jsonObject);
    }


    /**
     * 获取token
     */
    @Test
    public void getAccess_token(){
        System.out.println(wxappUtils.getAccessTokenByAppidAndSecret("wxd0c2a6a54163249b", "628792f7bdbe792dc5d940b85aa34e3e"));
    }
}